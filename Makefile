.PHONY: build-docker
docker-build:
	docker-compose build

.PHONY: build
build:
	docker-compose run --rm service
