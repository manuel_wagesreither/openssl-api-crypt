#pragma once
#include "crypto_types.hpp"


namespace crypto {

Message encrypt(const std::string& plaintext);
std::string decrypt(const Message& message);

} // namespace
