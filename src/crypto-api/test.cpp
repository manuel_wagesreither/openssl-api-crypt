#include "crypto_pub.hpp"

int main(int, char**)
{
	const std::string in = "asdfASDF0123!@#$";
	const std::string out = crypto::decrypt( crypto::encrypt( in ) );

    if( out == in )
        return 0;
    else
        return 1;
}
