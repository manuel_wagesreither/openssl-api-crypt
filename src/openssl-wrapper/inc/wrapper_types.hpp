#pragma once

// System headers
#include <string>

namespace ossl {


	class octet_string {
		typedef std::basic_string<unsigned char> store_t;
		const store_t s;

	public: 
		octet_string(const store_t::value_type* ptr, const std::size_t count) : s(ptr, count) {};
		octet_string(const std::string& str) : s((store_t::value_type*)str.data(), str.size()) {};

		const store_t::value_type* data() const { return s.data(); };
		const store_t::size_type size() const { return s.size(); };
		const store_t::size_type length() const { return s.length(); };

		std::string std_str() const { return std::string((char*)s.data(), s.size()); };
	};

// class string : private base_string
// {
// public: 
// 	string(const value_type* s, size_type count) : base_string(s, count) {};
// 	string(const std::string s) : base_string((value_type*)s.data(), s.size());
	
// 	data() const { base_string::data(); };
// 	length() const { base_string::length(); };

// 	std::string std_str() const { 
// };


class encrypted_pair
{
	const octet_string _ciphertext;
	const octet_string _tag;

public:
	encrypted_pair(
		const octet_string& ciphertext,
		const octet_string& tag) :
	_ciphertext{ciphertext},
	_tag{tag}
	{};

	octet_string ciphertext() const { return _ciphertext; };
	octet_string tag() const { return _tag; };
};

} // namespace
