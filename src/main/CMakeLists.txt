cmake_minimum_required( VERSION 3.13.4 )
set( CMAKE_CXX_STANDARD 14 )

add_executable( main )

set_target_properties( main
    PROPERTIES OUTPUT_NAME encrypt_decrypt )

target_sources( main
    PRIVATE     main.cpp )

target_link_libraries( main
    PRIVATE     crypto )

include( GNUInstallDirs )
install(
    TARGETS              main
    RUNTIME DESTINATION  ${CMAKE_INSTALL_BINDIR} )

# Copy executable to root of build tree
add_custom_command(
    TARGET main POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy
            ${CMAKE_CURRENT_BINARY_DIR}/encrypt_decrypt
            ${CMAKE_BINARY_DIR}/encrypt_decrypt )
