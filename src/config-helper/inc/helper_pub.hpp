#pragma once

// Project headers
#include "wrapper_types.hpp"

namespace config {

ossl::octet_string get_aad();
ossl::octet_string get_key();
ossl::octet_string get_iv();

}
